FROM scratch as build
COPY html/ /app

FROM nginx as development
RUN apt update
RUN apt install -y nano iputils-ping telnet
COPY --from=build /app /usr/share/nginx/html/

FROM nginx as production
COPY --from=build /app /usr/share/nginx/html/
